﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Pipopolam.Net.Tests
{
    [DataContract]
    public class ErrorResponse : IBasicResponse
    {
        public bool Success => Error == null;
        [DataMember] public string Error { get; set; }
        [DataMember] public int Code { get; set; }
    }

    [DataContract]
    public class ResponseWithRequiredField
    {
        [DataMember(IsRequired = true)] public string Message { get; set; }
    }

    public class BasicRequestsWebServiceWErrors : WebService<ErrorResponse>
    {
        public BasicRequestsWebServiceWErrors(bool critical = false) : base(critical) { }

        public string Host { get; set; }
        public override string BaseHost => Host;

        protected override void GenericServicePath(RequestBuilder builder)
        {
            builder.SetScheme(UrlScheme.Http);
        }

        public Request<BasicGetResponse> TestGet()
            => CreateRequest().AddPath("TestGet").Get<BasicGetResponse>();

        public Request<ResponseWithRequiredField> TestRequiredGet()
            => CreateRequest().AddPath("TestGet").Get<ResponseWithRequiredField>();
    }

    public class Connection_Problem_Prehandle_Errors
    {
        [Fact]
        public async Task Basic_Get_Is_Parsed()
        {
            var message = $"My legs are OK: {Guid.NewGuid():D}";
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    ctx.Response.StatusCode = 200;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write($"{{\"Message\":\"{message}\"}}");
                }
            };
            var api = new BasicRequestsWebServiceWErrors { Host = dummy.BaseUrl };
            var response = await api.TestGet();
            Assert.Equal(message, response.Message);
        }

        [Fact]
        public async Task Remote_Error_Throws_Wrex()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    ctx.Response.StatusCode = 500;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write("{\"Error\":\"UNKNOWN\"}");
                }
            };
            var api = new BasicRequestsWebServiceWErrors { Host = dummy.BaseUrl };
            await Assert.ThrowsAsync<WebServiceRemoteException<ErrorResponse>>(async () => await api.TestGet());
        }

        [Fact]
        public async Task Remote_Error_Throws_Wrex_On_200()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    ctx.Response.StatusCode = 200;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write("{\"Error\":\"UNKNOWN\"}");
                }
            };
            var api = new BasicRequestsWebServiceWErrors { Host = dummy.BaseUrl };
            await Assert.ThrowsAsync<WebServiceErrorException<ErrorResponse>>(async () => await api.TestGet());
        }

        [Fact]
        public async Task NoConnection()
        {
            var api = new BasicRequestsWebServiceWErrors { Host = "localhost:0/Temporary_Listen_Addresses/nonexisting" };
            await Assert.ThrowsAsync<WebServiceNoConnectionException>(async () => await api.TestGet());
        }

        [Fact]
        public async Task Connection_Timeout_If_Critical()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    Task.Delay(TimeSpan.FromSeconds(WebService.TIMEOUT)).Wait();
                    ctx.Response.StatusCode = 500;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write("{\"Error\":\"UNKNOWN\"}");
                }
            };
            var api = new BasicRequestsWebServiceWErrors(true) { Host = dummy.BaseUrl };
            await Assert.ThrowsAsync<WebServiceNoConnectionException>(async () => await api.TestGet());
        }

        [Fact]
        public async Task No_Timeout_If_Not_Critical()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    var wait = TimeSpan.FromSeconds(WebService.TIMEOUT) / 5;
                    ctx.Response.StatusCode = 200;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    Task.Delay(wait).Wait();
                    writer.Write($"{{\"Message\":\"A Very");
                    Task.Delay(wait).Wait();
                    writer.Write(" long");
                    Task.Delay(wait).Wait();
                    writer.Write(" message");
                    Task.Delay(wait).Wait();
                    writer.Write(" indeed.\"}");
                    Task.Delay(wait).Wait();
                }
            };
            var api = new BasicRequestsWebServiceWErrors(false) { Host = dummy.BaseUrl };
            var sw = Stopwatch.StartNew();
            var res = await api.TestGet();
            sw.Stop();
            Assert.True(sw.Elapsed > TimeSpan.FromSeconds(WebService.TIMEOUT));
            Assert.Equal("A Very long message indeed.", res.Message);
        }

        [Fact]
        public async Task Failed_Deserialization_Of_Reponse_And_Error_Throws_Meaningful_Ex()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    ctx.Response.StatusCode = 200;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write(@"{""Error"":null, ""Code"":""2718""}");
                }
            };
            var api = new BasicRequestsWebServiceWErrors(false) { Host = dummy.BaseUrl };
            await Assert.ThrowsAsync<WebServiceException>(async () => await api.TestRequiredGet());
        }



    }
}

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Xunit;

namespace Pipopolam.Net.Tests
{
    public class BasicRequestsWebService : WebService
    {
        public BasicRequestsWebService(bool critical = false) : base(critical){}

        public string Host { get; set; }
        public override string BaseHost => Host;

        protected override void GenericServicePath(RequestBuilder builder)
        {
            builder.SetScheme(UrlScheme.Http);
        }

        public Request<BasicGetResponse> TestGet()
            => CreateRequest().AddPath("TestGet").Get<BasicGetResponse>();
    }

    [DataContract]
    public class BasicGetResponse
    {
        [DataMember] public string Message { get; set; }
    }

    public class Connection_Problem_Errors
    {
        [Fact]
        public async Task Basic_Get_Is_Parsed()
        {
            var message = $"My legs are OK: {Guid.NewGuid():D}";
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    ctx.Response.StatusCode = 200;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write($"{{\"Message\":\"{message}\"}}");
                }
            };
            var api = new BasicRequestsWebService{ Host = dummy.BaseUrl };
            var response = await api.TestGet();
            Assert.Equal(message, response.Message);
        }

        [Fact]
        public async Task Remote_Error_Throws_Wrex()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    ctx.Response.StatusCode = 500;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write("{\"Error\":\"UNKNOWN\"}");
                }
            };
            var api = new BasicRequestsWebService { Host = dummy.BaseUrl };
            await Assert.ThrowsAsync<WebServiceRemoteException>(async () => await api.TestGet());
        }

        [Fact]
        public async Task NoConnection()
        {
            var api = new BasicRequestsWebService { Host = "localhost:0/Temporary_Listen_Addresses/nonexisting" };
            await Assert.ThrowsAsync<WebServiceNoConnectionException>(async () => await api.TestGet());
        }

        [Fact]
        public async Task Connection_Timeout_If_Critical()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    Task.Delay(TimeSpan.FromSeconds(WebService.TIMEOUT)).Wait();
                    ctx.Response.StatusCode = 500;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    writer.Write("{\"Error\":\"UNKNOWN\"}");
                }
            };
            var api = new BasicRequestsWebService(true) { Host = dummy.BaseUrl };
            await Assert.ThrowsAsync<WebServiceNoConnectionException>(async () => await api.TestGet());
        }

        [Fact]
        public async Task No_Timeout_If_Not_Critical()
        {
            var dummy = new LocalHttpServer()
            {
                OnRequest = (ctx) =>
                {
                    var wait = TimeSpan.FromSeconds(WebService.TIMEOUT) / 5;
                    ctx.Response.StatusCode = 200;
                    using var writer = new StreamWriter(ctx.Response.OutputStream);
                    Task.Delay(wait).Wait();
                    writer.Write($"{{\"Message\":\"A Very");
                    Task.Delay(wait).Wait();
                    writer.Write(" long");
                    Task.Delay(wait).Wait();
                    writer.Write(" message");
                    Task.Delay(wait).Wait();
                    writer.Write(" indeed.\"}");
                    Task.Delay(wait).Wait();
                }
            };
            var api = new BasicRequestsWebService(false) { Host = dummy.BaseUrl };
            var sw = Stopwatch.StartNew();
            var res = await api.TestGet();
            sw.Stop();
            Assert.True(sw.Elapsed > TimeSpan.FromSeconds(WebService.TIMEOUT));
            Assert.Equal("A Very long message indeed.", res.Message);
        }
    }
}

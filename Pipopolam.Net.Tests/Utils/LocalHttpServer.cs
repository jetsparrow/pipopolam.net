﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Pipopolam.Net.Tests
{
	public class LocalHttpServer : IDisposable
	{
		Task mainLoopTask;
		HttpListener listener;

		public string BaseUrl { get; }
		public string Id { get; } = $"{Guid.NewGuid():D}";

		public LocalHttpServer()
		{
			BaseUrl = $"localhost:80/Temporary_Listen_Addresses/{Id}";
			listener = new HttpListener();
			listener.Prefixes.Add($"http://+:80/Temporary_Listen_Addresses/{Id}/");
			listener.Start();
			mainLoopTask = MainLoop();
		}

		async Task MainLoop()
		{
			while (true)
			{
				var context = await listener.GetContextAsync();
				if (!context.Request.Url.ToString().Contains(Id))
					continue;
				OnRequest?.Invoke(context);
			}
		}

		public Action<HttpListenerContext> OnRequest { get; set; } = (ctx) =>
		{
			var req = ctx.Request;
			using var reader = new StreamReader(req.InputStream);
			var body = reader.ReadToEnd();

			var res = ctx.Response;
			res.StatusCode = 200;
			var writer = new StreamWriter(res.OutputStream);
			writer.Write("{}");
			writer.Close();
		};

        public void Dispose()
        {
			listener.Stop();
        }
    }

}
